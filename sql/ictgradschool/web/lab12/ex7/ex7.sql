DROP TABLE IF EXISTS soccer_player;

CREATE TABLE soccer_player (
 playerId INT NOT NULL,
 fname VARCHAR (60),
 natioanality VARCHAR (10),
 PRIMARY KEY (playerId)
);

INSERT INTO soccer_player (playerId,fname,natioanality) VALUES
 ('01', 'Peter','NZ'),
 ('02', 'jack','AU'),
 ('03', 'owen','US');


DROP TABLE IF EXISTS soccer_team;

CREATE TABLE soccer_team(
 TeamId INT NOT NULL,
 TeamName VARCHAR(60),
 city VARCHAR(60),
 captain CHAR(20),
 point CHAR(20),
 PRIMARY KEY (TeamId)
);

INSERT INTO soccer_team (TeamId, TeamName,city,captain,point) VALUES
 ('001', 'Auckland1','Auckland','peter','10'),
 ('002', 'Auckland2','Nelson','jack','20'),
 ('003', 'Auckland3','CHCH','owen','30');


CREATE TABLE soccer_league (
 TeamName VARCHAR(60),
 point CHAR(20),
PRIMARY KEY (TeamName)
);

INSERT INTO soccer_league (TeamName, point) VALUES
 ('Auckland1', '10'),
 ('Auckland2', '20'),
 ('Auckland3', '30');

CREATE TABLE  belongto_league(


  TeamName VARCHAR(60),
  TeamId INT,
 FOREIGN KEY (TeamId) REFERENCES soccer_team(TeamId),
  FOREIGN KEY (TeamName) REFERENCES soccer_league(TeamName)



);


CREATE TABLE  playBy(


  TeamId INT,
  playerId INT,
    FOREIGN KEY (playerId) REFERENCES soccer_player(playerId),
  FOREIGN KEY (TeamId) REFERENCES soccer_team(TeamId)

);

