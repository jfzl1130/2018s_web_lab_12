-- Answers to exercise 2 questions
SELECT fname,lname
FROM unidb_students AS s, unidb_attend AS a
WHERE s.id = a.id
      AND a.dept = 'comp'
      AND a.num = 219;

-- B

SELECT fname,lname
FROM unidb_students AS s, unidb_courses AS c
WHERE s.id = c.rep_id
      AND country <> 'NZ';

-- C

SELECT office
from unidb_lecturers AS l, unidb_courses AS c
where l.staff_no = c.coord_no
      and num=219;

-- D


SELECT DISTINCT s.fname,s.lname
from unidb_lecturers as l, unidb_attend as a, unidb_students as s ,unidb_teach as t
where l.staff_no= t.staff_no and t.dept = a.dept and t.num = a.num and a.id= s.id and  l.fname= 'Te Taka';


-- E

SELECT a.fname,a.lname, b.fname, b.lname
FROM unidb_students as a, unidb_students as b
where a.id= b.mentor;




-- F

SELECT fname,lname
FROM unidb_lecturers
where office like 'G%'
UNION
SELECT fname,lname
FROM unidb_students
WHERE country != "NZ";

-- G

SELECT coord_no,rep_id
FROM unidb_courses
where num=219;


SELECT fname,lname
FROM unidb_lecturers as l,unidb_courses as c
where c.coord_no = l.staff_no and c.dept = "comp" and c.num = "219"
UNION
SELECT fname,lname
FROM unidb_courses as c, unidb_students as s
WHERE c.rep_id= s.id and c.dept = "comp" and c.num = "219"


