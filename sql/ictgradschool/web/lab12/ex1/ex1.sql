-- Answers to exercise 1 questions
-- A

SELECT  DISTINCT dept
FROM unidb_courses;

-- B

SELECT semester
FROM unidb_attend;

--C

SELECT dept,num
FROM unidb_courses;

--D

SELECT fname,lname,country
FROM unidb_students
ORDER BY fname;

--E

SELECT fname,lname,mentor
FROM unidb_students
ORDER BY mentor;

--F

SELECT *
FROM unidb_lecturers
ORDER BY office;

--G

SELECT *
FROM unidb_lecturers
WHERE staff_no>500;

--H

SELECT *
FROM unidb_students
WHERE id>1668 and id<1824;

--I

SELECT *
FROM unidb_students
where  country= 'AU' OR country='NZ' OR country='US';

--J

SELECT *
FROM unidb_lecturers
where office like 'G%';

--K

SELECT *
FROM unidb_courses
where dept != 'comp';

--L

SELECT *
FROM unidb_students
where  country= 'FR' OR country='MX' ;